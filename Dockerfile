FROM node:12.6.0

WORKDIR /express/
COPY ./ /express/

RUN chmod +x wait-for-it.sh
RUN npm install

ENV EXPRESS_PORT 3000
ENV MONGO_HOST "localhost"
ENV MONGO_PORT 27017
ENV MONGO_DB_NAME "books_db"

EXPOSE ${EXPRESS_PORT}

CMD ./wait-for-it.sh ${MONGO_HOST}:${MONGO_PORT} -- \
    node server.js ${EXPRESS_PORT} ${MONGO_HOST} ${MONGO_PORT} ${MONGO_DB_NAME}
