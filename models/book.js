const mongoose = require('mongoose')

const schema = mongoose.Schema({
  title: String,
  author: String,
  description: String,
  image: String // Base64
})

module.exports = mongoose.model('Book', schema)
