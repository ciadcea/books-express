const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const mongoose = require('mongoose')

const Book = require('./models/book.js')

//
// Config
//

const express_port = process.argv[2] || 3000

const mongo_host = process.argv[3] || 'localhost'
const mongo_port = process.argv[4] || 27017
const mongo_db_name = process.argv[5] || 'books_db'

//
// Init server & connect to DB
//

const server = express()

server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())
server.use(cors())

mongoose.connect(`mongodb://${mongo_host}:${mongo_port}/${mongo_db_name}`, 
    { useNewUrlParser: true, useFindAndModify: false })

//
// Define routes
//

server.get('/books', function(request, response, next) {
  Book.find((error, found_books) => {
    if (error) {
      console.error(error)
      response.status(500).send(error)
    } else {
      response.json(found_books)
    }
  })
})

server.post('/book', (request, response) => {
  const newBook = request.body

  Book.create(newBook, (error, createdBook) => {
    if (error) {
      console.error(error)
      response.status(500).send(error)
    }

    response.status(201).json(createdBook)
  })
})

server.get('/book/:bookId', (request, response) => {
  const bookId = request.params.bookId

  Book.findById(bookId, (error, foundBook) => {
    if (error) {
      console.error(error)
      response.status(500).send(error)
    }

    response.status(200).json(foundBook)
  })
})

server.put('/book/:bookId', (request, response) => {
  const bookId = request.params.bookId
  const update = request.body

  Book.findByIdAndUpdate(bookId, update, (error, updatedBook) => {
    if (error) {
      console.error(error)
      response.status(500).send(error)
    }

    response.status(200).json(updatedBook)
  })
})

server.delete('/book/:bookId', (request, response) => {
  const bookId = request.params.bookId

  Book.findByIdAndRemove(bookId, (error, removedBook) => {
    if (error) {
      console.error(error)
      response.status(500).send(error)
    }

    response.status(200).json(removedBook)
  })
})

//
// Start server
//

server.listen(express_port, () => {
  console.log(`Book server listening on port ${express_port}`)
})
